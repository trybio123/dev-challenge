var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE article (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title text, 
            createdAt text, 
            updatedAt text,
            body text,
            archived INTEGER
            )`,
        (err) => {
            if (err) {
                // Table already created
            }else{
                // Table just created, creating some rows
                var insert = 'INSERT INTO article (title, createdAt, updatedAt, body, archived) VALUES (?,?,?,?,?)'
                db.run(insert, ["New Article","2019-01-01","2019-01-01","lorem ipsum dar der dor", 0])
            }
        }) 
    }
});


module.exports = db