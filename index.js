// Create express app
var express = require("express")
var app = express()
var db = require("./database.js")
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Server port
var HTTP_PORT = 8000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});

app.get("/tes/coba", (req, res, next) => {
    console.log("masuk ga?");
    res.json({"pesan":"masuk"})
});
// Insert here other API endpoints
app.get("/articles/read/:id", (req, res, next) => {
    var sql = "select * from article where id= ?"
    var params = [req.params.id]
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({
                "status":500,
                "message":"ERROR",
                "result":err.message
            });
            return;
        }
        res.json({
            "status":200,
            "message":"OK",
            "result":rows
        })
    });
});

// [POST] /articles/create
app.post("/articles/create/", (req, res, next) => {
    console.log(req.body.title);
    var errors=[]
    if (!req.body.title){
        errors.push("No title specified");
    }
    if (!req.body.body){
        errors.push("No body specified");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        title: req.body.title,
        createdAt: req.body.createdAt,
        updatedAt: req.body.createdAt,
        body: req.body.body,
        archived: req.body.archived,
        
    }
    var sql ='INSERT INTO article (title, createdAt, updatedAt, body, archived) VALUES (?,?,?,?,?)'
    var params =[data.title, data.createdAt, data.updatedAt, data.body, data.archived]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({
                "status": 500,
                "message": "OK",
                "result" : err.message
            })
            return;
        }
        res.json({
            "status": 300,
            "message": "OK",
            "result" : data
        })
    });
})

// [PATCH] /articles/update/{id}
app.patch("/articles/update/:id", (req, res, next) => {
    var data = {
        title: req.body.title,
        updatedAt: req.body.updatedAt,
        body: req.body.body,
        archived: req.body.archived,
    }
    // console.log(data);
    db.run(
        `UPDATE article set 
           title = coalesce(?,title), 
           updatedAt = COALESCE(?,updatedAt), 
           body = coalesce(?,body),
           archived = coalesce(?,archived) 
           WHERE id = ?`,
        [data.title, data.updatedAt, data.body, data.archived, req.params.id],
        (err, result) => {
            if (err){
                res.status(400).json({
                    "status": 500,
                    "message": "ERROR",
                    "result": res.message
                })
                console.log(err);
                return;
            }
            res.json({
                "status": 300,
                "message": "OK",
                "result": data
            })
        }
    );
})

// [DELETE] /articles/delete/{id}
app.delete("/articles/delete/:id", (req, res, next) => {
    db.run(
        'DELETE FROM article WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({
                    "status": 500,
                    "message":"ERROR",
                    "result": res.message
                })
                return;
            }
            res.json({
                "status": 300,
                "message":"OK", 
                "result": this.changes})
    });
})
// Root endpoint. jangan lupa nyimpen ini di bawah
app.get("/", (req, res, next) => {
    console.log("pastinya");
    res.json({"message":"testing testing"})
});
// Default response for any other request
app.use(function(req, res){
    res.status(404);
});


